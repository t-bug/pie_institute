//获取订单状态
function getOrderStatus(id) {
    return $axios({
        'url': `/api/wx-pay/getPayStatus/${id}`,
        'method': 'get',
    })
}

//发送支付请求
function wxPay(data) {
    return $axios({
        'url': '/api/wx-pay/native',
        'method': 'post',
        data
    })
}

//取消订单
function cancelOrderApi(data){
    return $axios({
        'url': '/api/wx-pay/cancel',
        'method': 'post',
        data
    })
}

//删除订单
function deleteOrderApi(data) {
    return $axios({
        'url': '/order/delete',
        'method': 'delete',
        data
    })
}

//申请退款
function refundApi(data) {
    return $axios({
        'url': '/api/wx-pay/refund',
        'method': 'post',
        data
    })
}
//提交订单
function  addOrderApi(data){
    return $axios({
        'url': '/order/submit',
        'method': 'post',
        data
    })
}

//查询所有订单
function orderListApi() {
  return $axios({
    'url': '/order/list',
    'method': 'get',
  })
}

//分页查询订单
function orderPagingApi(data) {
  return $axios({
      'url': '/order/userPage',
      'method': 'get',
      params:{...data}
  })
}

//再来一单
function orderAgainApi(data) {
  return $axios({
      'url': '/order/again',
      'method': 'post',
      data
  })
}