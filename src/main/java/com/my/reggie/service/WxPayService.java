package com.my.reggie.service;

import com.my.reggie.pojo.Orders;
import com.wechat.pay.contrib.apache.httpclient.notification.Notification;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

public interface WxPayService {
    Map nativePay(Orders orders, HttpSession session) throws IOException;

    void processOrder(Notification notification);

    void cancelOrder(Orders order) throws Exception;

    void closeOrder(String orderNo) throws Exception;

    String queryOrder(String orderNo) throws Exception;

    void checkOrderStatus(String orderNo) throws Exception;

    void refund(Orders order) throws Exception;

    void processRefund(Notification notification);

    String queryRefund(String orderNo) throws Exception;

    void checkRefundStatus(String orderNo) throws Exception;
}
