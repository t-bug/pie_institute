package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.Orders;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface OrdersService extends IService<Orders> {
    /**
     * 添加订单
     * @param orders
     */
    public Orders createOrder(Orders orders, HttpSession session);

    public void updateStatusByOrderNo(String id,String status);

    Integer getOrderStatus(String orderNumber);

    List<Orders> getNoPayOrderOvertime(int minutes);

    List<Orders> getProcessingOvertime(int minutes);
}
