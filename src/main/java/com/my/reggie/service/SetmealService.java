package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.Setmeal;

public interface SetmealService extends IService<Setmeal> {
}
