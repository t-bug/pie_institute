package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.PaymentInfo;

public interface PaymentInfoService extends IService<PaymentInfo> {
    void saveInfo(String decryptData);

    void updateStatus(String number,String status);
}
