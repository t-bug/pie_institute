package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
