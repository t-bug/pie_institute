package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
