package com.my.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.my.reggie.common.CustomException;
import com.my.reggie.mapper.CategoryMapper;
import com.my.reggie.pojo.Category;
import com.my.reggie.pojo.Dish;
import com.my.reggie.pojo.Setmeal;
import com.my.reggie.service.CategoryService;
import com.my.reggie.service.DishService;
import com.my.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Autowired
    private DishService dishService;
    @Autowired
    private SetmealService setmealService;

    /**
     * 自定义删除
     * @param id
     */
    @Override
    public void delete(Long id) {
        LambdaQueryWrapper<Dish> lqw1 = new LambdaQueryWrapper<>();
        lqw1.eq(Dish::getCategoryId,id);
        long count1 = dishService.count(lqw1);
        if(count1 > 0){
            //关联了菜品，不能删除
            throw new CustomException("该分类关联了其他菜品，不能删除！");
        }
        LambdaQueryWrapper<Setmeal> lqw2 = new LambdaQueryWrapper<>();
        lqw2.eq(Setmeal::getCategoryId,id);
        long count2 = setmealService.count(lqw2);
        if(count2 > 0){
            //关联了套餐，不能删除
            throw new CustomException("该分类关联了其他套餐，不能删除！");
        }
        //正常删除
        super.removeById(id);
    }
}
