package com.my.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.my.reggie.enums.wxpay.WxRefundStatus;
import com.my.reggie.mapper.RefundInfoMapper;
import com.my.reggie.pojo.Orders;
import com.my.reggie.pojo.RefundInfo;
import com.my.reggie.service.RefundInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class RefundInfoServiceImpl extends ServiceImpl<RefundInfoMapper, RefundInfo> implements RefundInfoService {
    /**
     * 创建退款记录
     * @param order
     * @param refundNumber
     */
    @Override
    public void createInfo(Orders order, String refundNumber) {
        log.info("创建退款记录...");

        RefundInfo refundInfo = new RefundInfo();
        int amount = Integer.parseInt(String.valueOf(order.getAmount()));
        refundInfo.setOrderNumber(order.getNumber());
        refundInfo.setRefundNo(refundNumber);
        refundInfo.setRefundTotal(amount);
        refundInfo.setStatus(WxRefundStatus.PROCESSING.getType());

        this.save(refundInfo);
    }
}
