package com.my.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.my.reggie.mapper.DIshFlavorMapper;
import com.my.reggie.pojo.DishFlavor;
import com.my.reggie.service.DishFlavorService;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DIshFlavorMapper, DishFlavor> implements DishFlavorService {
}
