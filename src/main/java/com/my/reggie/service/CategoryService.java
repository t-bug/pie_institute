package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.Category;

public interface CategoryService extends IService<Category> {
    void delete(Long id);
}
