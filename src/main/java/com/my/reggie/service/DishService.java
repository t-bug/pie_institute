package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.Dish;

public interface DishService extends IService<Dish> {
}
