package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
