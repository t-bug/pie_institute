package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {
}
