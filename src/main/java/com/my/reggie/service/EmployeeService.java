package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.Employee;

public interface EmployeeService extends IService<Employee> {
}
