package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.Orders;
import com.my.reggie.pojo.RefundInfo;


public interface RefundInfoService extends IService<RefundInfo> {
    void createInfo(Orders order, String refundNumber);
}
