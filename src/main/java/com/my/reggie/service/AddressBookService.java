package com.my.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.my.reggie.pojo.AddressBook;

public interface AddressBookService extends IService<AddressBook> {
}
