package com.my.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.my.reggie.pojo.Setmeal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
