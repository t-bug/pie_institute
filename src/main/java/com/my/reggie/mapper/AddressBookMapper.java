package com.my.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.my.reggie.pojo.AddressBook;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AddressBookMapper extends BaseMapper<AddressBook> {
}
