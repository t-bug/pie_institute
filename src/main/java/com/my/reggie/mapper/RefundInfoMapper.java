package com.my.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.my.reggie.pojo.RefundInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RefundInfoMapper extends BaseMapper<RefundInfo> {
}
