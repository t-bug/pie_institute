package com.my.reggie.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PaymentInfo {
    private static final long serialVersionUID = 1L;

    private Long id;    //日志id

    private String orderNumber;   //订单id

    private String transactionId;  //支付系统交易编号

    private String tradeType;   //交易类型

    private String tradeState;   //交易状态

    private Integer payerTotal;  //交易金额

    private LocalDateTime createTime;  //创建时间

    private LocalDateTime updateTime;  //更新时间
}
