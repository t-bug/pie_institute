package com.my.reggie.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RefundInfo {
    private static final long serialVersionUID = 1L;

    private Long id;    //退款记录id

    private String orderNumber;  //订单号

    private String refundNo;    //退款编号

    private Integer refundTotal;  //退款金额

    private String status;      //退款状态

    private LocalDateTime createTime;  //创建时间

    private LocalDateTime updateTime;  //更新时间
}
