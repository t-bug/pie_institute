package com.my.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.my.reggie.common.R;
import com.my.reggie.pojo.ShoppingCart;
import com.my.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 获取购物车商品
     * @return
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> list(HttpSession session){
        //获取当前用户id
        Long userId = (Long) session.getAttribute("user");
        LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getUserId,userId);
        //查询当前用户的购物车信息
        List<ShoppingCart> list = shoppingCartService.list(lqw);
        return R.success(list);
    }

    /**
     * 添加购物车
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart, HttpSession session){
        //设置当前菜品的用户id
        Long userId = (Long) session.getAttribute("user");
        shoppingCart.setUserId(userId);
        LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getUserId,userId);
        lqw.eq(shoppingCart.getDishId() != null,ShoppingCart::getDishId,shoppingCart.getDishId());
        lqw.eq(shoppingCart.getSetmealId() != null,ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        //查询购物车是否含有该菜品or套餐
        ShoppingCart dish = shoppingCartService.getOne(lqw);
        if(dish == null) {
            //购物车不包含该菜品
            //设置菜品数为1
            shoppingCart.setNumber(1);
            shoppingCartService.save(shoppingCart);
            //令dish = shoppingCart，用于返回
            dish = shoppingCart;
        } else {
            //获取当前菜品数量
            Integer number = dish.getNumber();
            //当前菜品数量加1
            number += 1;
            //更新当前菜品数量
            dish.setNumber(number);
            shoppingCartService.updateById(dish);
        }
        return R.success(dish);
    }

    /**
     * 减少菜品数
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    public R<ShoppingCart> sub(@RequestBody ShoppingCart shoppingCart){
        Long dishId = shoppingCart.getDishId();   //获取当前菜品id
        Long setmealId = shoppingCart.getSetmealId();  //获取套餐id
        LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();
        lqw.eq(dishId != null,ShoppingCart::getDishId,dishId);
        lqw.eq(setmealId != null,ShoppingCart::getSetmealId,setmealId);
        ShoppingCart shoppingDish = shoppingCartService.getOne(lqw);
        if(shoppingDish.getNumber() <= 1) {
            //购物车中当前菜品数为1，直接删除
            shoppingCart.setNumber(0);
            shoppingCartService.remove(lqw);
            shoppingDish = shoppingCart;
        } else {
            //菜品数量减1
            Integer number = shoppingDish.getNumber();
            number -= 1;
            //更新菜品数量
            shoppingDish.setNumber(number);
            shoppingCartService.updateById(shoppingDish);
        }
        return R.success(shoppingDish);
    }

    /**
     * 清空购物车
     * @param session
     * @return
     */
    @DeleteMapping("/clean")
    public R<String> clean(HttpSession session){
        Long userId = (Long) session.getAttribute("user");
        LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getUserId,userId);
        shoppingCartService.remove(lqw);
        return R.success("删除成功");
    }
}
