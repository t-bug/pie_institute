package com.my.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.my.reggie.common.R;
import com.my.reggie.dto.OrdersDto;
import com.my.reggie.pojo.OrderDetail;
import com.my.reggie.pojo.Orders;
import com.my.reggie.service.OrderDetailService;
import com.my.reggie.service.OrdersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@Api(tags = "商品订单管理")
@RequestMapping("/order")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private OrderDetailService orderDetailService;

    /**
     * 获取订单状态(用于前端定时查询功能)
     * 已弃用，改成后台主动向微信服务器查询订单状态
     * @param id
     * @return
     */
    @Deprecated
    @ApiOperation("查询本地订单状态")
    @GetMapping("/getOrderStatus/{id}")
    public R<String> getStatus(@PathVariable String id){
        int status = ordersService.getOrderStatus(id);
        if(status == 2) {
            return R.success("支付成功");  //支付成功
        }
        R<String> r = new R<>();
        r.setCode(101);
        r.setMsg("支付中......");
        return r;
    }

    /**
     * 分页查询(管理端)
     * @param page  当前页号
     * @param pageSize  每页显示条数
     * @param number  订单号
     * @param beginTime  订单开始时间
     * @param endTime  订单结束时间
     * @return  R<Page<Orders>>
     */
    @GetMapping("/page")
    public R<Page<Orders>> page(int page, int pageSize, String number, String beginTime,String endTime){
        Page<Orders> info = new Page<>(page,pageSize);
        LambdaQueryWrapper<Orders> lqw = new LambdaQueryWrapper<>();
        lqw.eq(number != null,Orders::getNumber,number);
        lqw.ge(beginTime != null,Orders::getOrderTime,beginTime);
        lqw.le(endTime != null,Orders::getOrderTime,endTime);
        lqw.orderByDesc(Orders::getOrderTime);
        ordersService.page(info,lqw);
        return R.success(info);
    }

    /**
     * 更改订单状态
     * @param orders
     * @return
     */
    @PutMapping
    public R<String> status(@RequestBody Orders orders) {
        ordersService.updateById(orders);
        return R.success("更改订单状态成功");
    }

    /**
     * 订单分页查询(手机端)
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public R<Page<OrdersDto>> userPage(int page, int pageSize, HttpSession session){
        //获取当前用户id
        Long userId = (Long) session.getAttribute("user");
        Page<Orders> pageInfo = new Page<>(page,pageSize);
        Page<OrdersDto> dtoPage = new Page<>();
        LambdaQueryWrapper<Orders> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Orders::getUserId,userId);
        lqw.orderByDesc(Orders::getOrderTime); //按照订单时间排序
        ordersService.page(pageInfo,lqw);
        //拷贝属性信息
        BeanUtils.copyProperties(pageInfo,dtoPage,"records");
        List<OrdersDto> orderDetailList = new ArrayList<>();
        for (Orders record : pageInfo.getRecords()) {
            OrdersDto ordersDto = new OrdersDto();
            BeanUtils.copyProperties(record,ordersDto);
            LambdaQueryWrapper<OrderDetail> ODLqw = new LambdaQueryWrapper<>();
            ODLqw.eq(OrderDetail::getOrderId,record.getNumber());
            List<OrderDetail> list = orderDetailService.list(ODLqw);
            ordersDto.setOrderDetails(list);
            orderDetailList.add(ordersDto);
        }
        dtoPage.setRecords(orderDetailList);
        return R.success(dtoPage);
    }

    /**
     * 提交订单
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders,HttpSession session) {
        log.info("订单信息：{}",orders);
        ordersService.createOrder(orders,session);
        return R.success("下单成功");
    }

    /**
     * 删除订单
     * @param order
     * @return
     */
    @Transactional
    @DeleteMapping("/delete")
    public R<String> delete(@RequestBody Orders order) {
        log.info("删除订单===>{}",order.getNumber());
        ordersService.removeById(order.getId());
        LambdaQueryWrapper<OrderDetail> lqw = new LambdaQueryWrapper<>();
        lqw.eq(OrderDetail::getOrderId,order.getNumber());
        orderDetailService.remove(lqw);
        return R.success("删除订单成功");
    }

    @PostMapping("/again")
    public R<String> again(@RequestBody Orders order) {
        log.info("Order==>{}",order);
        return R.success("again");
    }
}
