package com.my.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.my.reggie.common.BaseContext;
import com.my.reggie.common.R;
import com.my.reggie.pojo.AddressBook;
import com.my.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/addressBook")
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;

    /**
     * 获取地址列表
     * @return
     */
    @GetMapping("/list")
    public R<List<AddressBook>> list(HttpSession session){
        LambdaQueryWrapper<AddressBook> lqw = new LambdaQueryWrapper<>();
        Long userId = (Long) session.getAttribute("user");
        lqw.eq(AddressBook::getUserId,userId);
        List<AddressBook> list = addressBookService.list(lqw);
        return R.success(list);
    }

    /**
     * 添加地址
     * @param addressBook
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody AddressBook addressBook, HttpSession session){
        Long userId = (Long) session.getAttribute("user");
        log.info("当前登录用户id：{}",userId);
        addressBook.setUserId(userId);
        addressBookService.save(addressBook);
        return R.success("添加成功");
    }

    /**
     * 根据id获取地址信息
     * @param addressId
     * @return
     */
    @GetMapping("/{addressId}")
    public R<AddressBook> getById(@PathVariable Long addressId){
        AddressBook address = addressBookService.getById(addressId);
        return R.success(address);
    }

    /**
     * 删除地址
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> delete(Long ids){
        LambdaQueryWrapper<AddressBook> lqw = new LambdaQueryWrapper<>();
        lqw.eq(AddressBook::getId,ids);
        addressBookService.remove(lqw);
        return R.success("删除成功");
    }

    /**
     * 修改地址
     * @param addressBook
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody AddressBook addressBook) {
        log.info("执行地址修改程序...");
        addressBookService.updateById(addressBook);
        return R.success("修改成功");
    }

    /**
     * 修改默认地址
     * @param addressBook
     * @return
     */
    @PutMapping("/default")
    public R<String> setDefault(@RequestBody AddressBook addressBook,HttpSession session){
        //获取当前用户id
        Long userId = (Long) session.getAttribute("user");
        //将原来的默认地址取消
        LambdaUpdateWrapper<AddressBook> luw = new LambdaUpdateWrapper<>();
        luw.eq(AddressBook::getUserId, userId);
        luw.set(AddressBook::getIsDefault,0);
        addressBookService.update(luw);
        //重新设置默认地址
        addressBook.setIsDefault(1);
        addressBookService.updateById(addressBook);
        return R.success("设置成功");
    }

    /**
     * 获取默认地址(结算使用)
     * @return
     */
    @GetMapping("/default")
    public R<AddressBook> getDefault(HttpSession session){
        Long userId = (Long) session.getAttribute("user");
        LambdaQueryWrapper<AddressBook> lqw = new LambdaQueryWrapper<>();
        lqw.eq(AddressBook::getUserId,userId);
        lqw.eq(AddressBook::getIsDefault,1);
        AddressBook defaultAddress = addressBookService.getOne(lqw);
        log.info("默认地址：{}",defaultAddress);
        return R.success(defaultAddress);
    }
}
