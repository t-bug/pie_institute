package com.my.reggie.dto;

import com.my.reggie.pojo.SetmealDish;
import lombok.Data;

@Data
public class SetmealDishDto extends SetmealDish {
    private String image;
}
