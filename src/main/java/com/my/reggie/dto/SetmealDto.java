package com.my.reggie.dto;

import com.my.reggie.pojo.Setmeal;
import com.my.reggie.pojo.SetmealDish;
import lombok.Data;

import java.util.List;

@Data
public class SetmealDto extends Setmeal {
    private  List<SetmealDish> setmealDishes;

    private String categoryName;
}
