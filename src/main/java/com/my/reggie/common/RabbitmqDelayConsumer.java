package com.my.reggie.common;

import com.my.reggie.service.WxPayService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class RabbitmqDelayConsumer {
    @Autowired
    private WxPayService wxPayService;

    /**
     * 监听订单延迟队列
     * @param orderNo
     * @throws Exception
     */
    @RabbitListener(queues = {"plugin.delay.order.queue"})
    public void orderDelayQueue(String orderNo, Message message, Channel channel) throws Exception {
        log.info("订单延迟队列开始消费...");

        try {
            //处理订单
            wxPayService.checkOrderStatus(orderNo);
            //告诉服务器收到这条消息 已经被我消费了 可以在队列删掉 这样以后就不会再发了 否则消息服务器以为这条消息没处理掉 后续还会在发
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            log.info("消息接收成功");
        } catch (Exception e) {
            e.printStackTrace();
            //消息重新入队
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,true);
            log.info("消息接收失败，重新入队");
        }
    }

    /**
     * 监听退款延迟队列
     * @param refundNo
     */
    @RabbitListener(queues = {"plugin.delay.refund.queue"})
    public void refundDelayQueue(String refundNo, Message message, Channel channel) throws Exception {
        log.info("退款延迟队列开始消费...");
        try {
            //处理退款信息
            wxPayService.checkRefundStatus(refundNo);
            //告诉服务器收到这条消息 已经被我消费了 可以在队列删掉 这样以后就不会再发了 否则消息服务器以为这条消息没处理掉 后续还会在发
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            log.info("消息接收成功");
        } catch (Exception e) {
            e.printStackTrace();
            //消息重新入队
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,true);
            log.info("消息接收失败，重新入队");
        }
    }
}
