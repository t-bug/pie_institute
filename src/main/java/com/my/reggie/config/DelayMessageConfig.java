package com.my.reggie.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class DelayMessageConfig {

    //交换机名称
    public static final String DELAY_EXCHANGE_NAME = "plugin.delay.exchange";

    //消息队列名称
    public static final String DELAY_QUEUE_ORDER_NAME = "plugin.delay.order.queue";  //订单队列
    public static final String DELAY_QUEUE_REFUND_NAME = "plugin.delay.refund.queue";  //退款处理队列

    //路由名称
    public static final String ROUTING_KEY_ORDER = "plugin.delay.routing_order";  //订单路由名称
    public static final String ROUTING_KEY_REFUND = "plugin.delay.routing_refund";  //退款路由名称

    /**
     * 声明一个订单延迟队列
     * @return
     */
    @Bean("ORDER_DELAY_QUEUE")
    Queue orderDelayQueue(){
        return QueueBuilder.durable(DELAY_QUEUE_ORDER_NAME).build();
    }
    /**
     * 声明一个退款延迟队列
     * @return
     */
    @Bean("REFUND_DELAY_QUEUE")
    Queue refundDelayQueue(){
        return QueueBuilder.durable(DELAY_QUEUE_REFUND_NAME).build();
    }

    /**
     * 声明一个交换机
     * @return
     */
    @Bean("DELAY_EXCHANGE")
    CustomExchange delayExchange(){
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(DELAY_EXCHANGE_NAME, "x-delayed-message", true,false, args);
    }

    /**
     * 订单延迟队列绑定
     * @param orderDelayQueue
     * @param delayExchange
     * @return
     */
    @Bean
    Binding orderDelayQueueBinding(@Qualifier("ORDER_DELAY_QUEUE") Queue orderDelayQueue,@Qualifier("DELAY_EXCHANGE") CustomExchange delayExchange){
        return BindingBuilder.bind(orderDelayQueue).to(delayExchange).with(ROUTING_KEY_ORDER).noargs();
    }

    /**
     * 退款延迟队列绑定
     * @param refundDelayQueue
     * @param delayExchange
     * @return
     */
    @Bean
    Binding refundDelayQueueBinding(@Qualifier("REFUND_DELAY_QUEUE") Queue refundDelayQueue,@Qualifier("DELAY_EXCHANGE") CustomExchange delayExchange){
        return BindingBuilder.bind(refundDelayQueue).to(delayExchange).with(ROUTING_KEY_REFUND).noargs();
    }
}

