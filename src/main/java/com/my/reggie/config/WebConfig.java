/*
package com.my.reggie.config;

import com.my.reggie.config.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(loginInterceptor);
        registration.addPathPatterns("/**"); // 所有路径都被拦截
        registration.excludePathPatterns(    // 添加不拦截路径
                "/employee/login",                    // 登录路径
                "/employee/logout",                  //退出登录
                "/backend/**",              // 静态资源
                "/front/**"                 // 静态资源
        );
    }
}
*/
