package com.my.reggie.task;

import com.my.reggie.pojo.Orders;
import com.my.reggie.service.OrdersService;
import com.my.reggie.service.WxPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class WxPayTask {
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private WxPayService wxPayService;
    /*
     * Scheduled()
     * 秒 分 时 日 月 周
     * 以秒为例
     * *：每秒都执行
     * 1-3：从第1秒开始执行，到第3秒结束执行
     * 0/3：从第0秒开始，每隔3秒执行1次
     * 1,2,3：在指定的第1、2、3秒执行
     * ?：不指定
     * 日和周不能同时制定，指定其中之一，则另一个设置为?
     */

    /**
     * 每隔2小时查询一次，查询已经下单但是还没支付的订单
     */
    @Scheduled(cron = "0 0 0/2 * * ?")
    public void orderConfirm() throws Exception {
        log.info("orderConfirm被执行....");

        List<Orders> list = ordersService.getNoPayOrderOvertime(5);

        for (Orders order : list) {
            String OrderNo = order.getNumber();
            log.info("查询到超时订单==>{}",OrderNo);

            //核实订单状态：调用微信支付查询接口
            wxPayService.checkOrderStatus(OrderNo);
        }
    }

    /**
     * 每隔2小时查询一次，查询已经申请退款但是还没收到通知的订单
     */
    @Scheduled(cron = "0 0 0/2 * * ?")
    public void refundConfirm() throws Exception {
        log.info("refundConfirm被执行....");

        List<Orders> list = ordersService.getProcessingOvertime(5);

        for (Orders order : list) {
            String refundNo = order.getRefundNumber();
            log.info("查询到退款超时订单==>{}",refundNo);

            //核实订单状态：调用微信支付查询接口
            wxPayService.checkRefundStatus(refundNo);
        }
    }
}
