package com.my;

import com.my.reggie.ReggieApplication;
import com.my.reggie.config.WxPayConfig;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.security.PrivateKey;

@SpringBootTest(classes = {ReggieApplication.class})
class ReggieApplicationTests {
    @Resource
    private WxPayConfig wxPayConfig;
    @Test
    void testGetPrivateKey() {
        //获取私钥路径
        String privateKeyPath = wxPayConfig.getPrivateKeyPath();

        //获取私钥
        PrivateKey privateKey = wxPayConfig.getPrivateKey(privateKeyPath);

        System.out.println(privateKey);
    }

}
