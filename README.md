# Pie Institute

#### 介绍
外卖项目，包含前后端部分，还实现了微信支付功能

#### 软件架构
使用的技术栈为：Springboot + ElementUI + Vue + Redis + Mybatis-Plus + RabbitMQ，数据库采用的是MySql

#### 安装教程

1.  安装Redis
2.  安装RabbitMQ
3.  安装MySql

#### 使用说明

1.  安装好上述软件并正确配置之后可以直接运行


